#!/usr/bin/env python3

def adj_for_fuel(c):
    total = c
    while int(c / 3) - 2 > 0:
        c = int(c / 3) - 2
        total += c
    return total

with open("in.txt", "r") as f:
    data = f.read()

masses = [int(d) for d in data.split()]
costs = [int(m / 3) - 2 for m in masses]

costs = [adj_for_fuel(c) for c in costs]
print(sum(costs))
