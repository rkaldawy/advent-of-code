#!/usr/bin/env python3
import copy


class VM:
    def __init__(self, code):
        self.code = copy.deepcopy(code)
        self.memory = copy.deepcopy(code)
        self.pc = 0
        self.modes = []
        self.instr_map = {
            1: self.add,
            2: self.mult,
            3: self.store,
            4: self.prnt,
            5: self.jt,
            6: self.jf,
            7: self.lt,
            8: self.eq,
            99: self.halt
        }

    def handle_param(self):
        m = self.next_mode()

        if m: return self.next_code()
        else: return self.memory[self.next_code()]

    def add(self):
        a = self.handle_param()
        b = self.handle_param()
        self.memory[self.next_code()] = a + b
        return 0

    def mult(self):
        a = self.handle_param()
        b = self.handle_param()
        self.memory[self.next_code()] = a * b
        return 0

    def store(self):
        a = int(input())
        self.memory[self.next_code()] = a
        return 0

    def prnt(self):
        a = self.handle_param()
        print(a)
        return 0

    def jt(self):
        a = self.handle_param()
        b = self.handle_param()
        if a: self.pc = b
        return 0

    def jf(self):
        a = self.handle_param()
        b = self.handle_param()
        if not a: self.pc = b
        return 0

    def lt(self):
        a = self.handle_param()
        b = self.handle_param()
        self.memory[self.next_code()] = int(a < b)
        return 0

    def eq(self):
        a = self.handle_param()
        b = self.handle_param()
        self.memory[self.next_code()] = int(a == b)
        return 0

    def halt(self):
        return 1

    def next_code(self):
        ret = self.memory[self.pc]
        self.pc += 1
        return ret

    def parse_opcode(self, code):
        opcode = code % 100
        code = int(code / 100)

        self.modes = []
        while code > 0:
            self.modes += [code % 10]
            code = int(code / 10)

        return opcode

    def next_mode(self):
        if not len(self.modes):
            return 0
        ret = self.modes[0]
        self.modes = self.modes[1:]
        return ret

    def write(self, pos, val):
        self.memory[pos] = val

    def __getitem__(self, key):
        return self.memory[key]

    def reset(self):
        self.memory = copy.deepcopy(self.code)
        self.pc = 0

    def run(self):
        while (True):
            try:
                opcode = self.parse_opcode(self.next_code())
                try:
                    if self.instr_map[opcode]():
                        return
                except KeyError:
                    print("Error: unknown opcode " + str(opcode))
                    return -1
            except IndexError:
                print("Error: ran off the edge of memory.")
                return -1


class DebugVM(VM):
    def __init__(self, code, inputs=[], breakpoints=[]):
        self.inputs = inputs
        self.outputs = []
        self.breakpoints = breakpoints
        VM.__init__(self, code)

    def store(self):
        a = self.inputs[0]
        self.inputs = self.inputs[1:]
        self.memory[self.next_code()] = a
        return 0

    def prnt(self):
        a = self.handle_param()
        self.outputs += [a]
        return 0

    def append_input(self, val):
        self.inputs += [val]

    def read_output(self):
        try:
            ret = self.outputs[0]
            self.outputs = self.outputs[1:]
            return ret
        except:
            return None

    def step(self):
        try:
            opcode = self.parse_opcode(self.next_code())
            try:
                if self.instr_map[opcode]():
                    return 0
            except KeyError:
                print("Error: unknown opcode " + str(opcode))
                return -1
        except IndexError:
            print("Error: ran off the edge of memory.")
            return -1

    def cont(self):
        self.step()
        return self.run()

    def run(self):
        while (True):
            try:
                opcode = self.parse_opcode(self.next_code())
                if opcode in self.breakpoints:
                    self.pc -= 1
                    return opcode
                try:
                    if self.instr_map[opcode]():
                        return 0
                except KeyError:
                    print("Error: unknown opcode " + str(opcode))
                    return -1
            except IndexError:
                print("Error: ran off the edge of memory.")
                return -1
