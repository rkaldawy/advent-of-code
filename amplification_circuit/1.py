#!/usr/bin/env python3

import itertools

from vm import *

with open("in.txt", "r") as f:
    code = f.read()
code = [int(c) for c in code.strip().split(",")]

phases = [i for i in range(0, 5)]
c = list(itertools.permutations(phases, len(phases)))
b = [3, 4]


def iterate_vm(vm, sig, phase):
    if not vm.run(): return None
    vm.append_input(phase)
    vm.cont()
    vm.append_input(sig)
    vm.cont()
    vm.step()
    return vm.read_output()


'''
        if vm[idx] is None:
            sigs[((idx + 1) % 5)] = sig 
            idx += 1
            continue
'''


def calculate_signal(c):
    vms = [DebugVM(code, breakpoints=b) for i in range(0, 5)]
    sigs = [0 for i in range(0, 5)]
    idx = 0

    while sum(x is not None for x in vms) > 0:
        vm, sig, phase = vms[idx], sigs[idx], c[idx]
        new = iterate_vm(vm, sig, phase)

        idx = (idx + 1) % 5
        if new is None:
            vms[idx] = None
            sigs[idx] = sig
        else:
            sigs[idx] = new

    print(sigs[4])
    return sigs[4]


max_sig = 0
for _ in c:
    sig = calculate_signal(_)
    if sig > max_sig:
        max_sig = sig

print(max_sig)
