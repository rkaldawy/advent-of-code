#!/usr/bin/env python3

import itertools
import time

from vm import *


def init_vms(vms, phases):
    for i in range(0, 5):
        ret = vms[i].run()
        vms[i].append_input(phases[i])
        vms[i].step()


def iterate_vm(vm, sig):
    if not vm.run(): return None
    vm.append_input(sig)
    v = vm.cont()
    vm.step()
    return vm.read_output()


def calculate_signal(c):
    b = [3, 4]
    vms = [DebugVM(code, inputs=[], breakpoints=b) for i in range(0, 5)]
    init_vms(vms, c)
    sigs = [0 for i in range(0, 5)]
    idx = 0

    while sum(x is not None for x in vms) > 0:
        vm, sig = vms[idx], sigs[idx]
        print(sig)
        new = iterate_vm(vm, sig)

        idx = (idx + 1) % 5
        print(idx, new)
        if new is None:
            vms[(idx - 1) % 5] = None
        else:
            sigs[idx] = new

    return sigs[4]


with open("in.txt", "r") as f:
    code = f.read()
code = [int(c) for c in code.strip().split(",")]

phases = [i + 5 for i in range(0, 5)]
c = list(itertools.permutations(phases, len(phases)))

#vm = VM(code)
#vm.run()

max_sig = 0
for _ in c:
    sig = calculate_signal(_)
    if sig > max_sig:
        max_sig = sig

print(max_sig)
