#!/usr/bin/env python3

def count_locations(data):
    l = {}

    s = 0
    x, y = 0,  0

    for elt in data:
        d = int(elt[1:])
        for i in range(0, d):
            l[(x, y)] = s
            if elt[0] == "R":
                x += 1
            elif elt[0] == "L":
                x -= 1
            elif elt[0] == "D":
                y += 1
            elif elt[0] == "U":
                y -= 1
            s += 1

    return l

def solution(data):
    l1, l2 = count_locations(data[0].split(",")), count_locations(data[1].split(","))
    c = [l1[k] + l2[k] for k in l1.keys() & l2.keys()]
    c.sort()
    print(c)
    print(c[1])

with open("in.txt", "r") as f:
    data = f.read()
data = data.strip().split("\n")

solution(data)
