import operator

class Segment:
    def __init__(self,  lwr, upr, val):
        self.lwr = lwr
        self.upr = upr
        self.val = val

class Segments:
    def __init__(self, code):
        self.v = []
        self.h = []
        self.generate_segments(code)

    def generate_segments(self, code):
        code = code.strip().split(",")
        x, y = 0, 0

        for elt in code:
            val = int(elt[1:])

            if elt[0] == "R":
                self.h += [Segment(x, x + val, y)]
                x += val
            elif elt[0] == "L":
                self.h += [Segment(x - val, x, y)]
                x -= val
            elif elt[0] == "U":
                self.v += [Segment(y - val, y, x)]
                y -= val
            elif elt[0] == "D":
                self.v += [Segment(y, y + val, x)]
                y += val

        self.h.sort(key=operator.attrgetter('val'))
        self.v.sort(key=operator.attrgetter('val'))

    def find_intersections(self, o):
        i = []
        for h in self.h:
            for v in o.v:
                if v.val < h.lwr:
                    continue
                elif v.val > h.upr:
                    break
                elif v.lwr > h.val or v.upr < h.val:
                    continue
                else:
                    i += [(v.val, h.val)]

        for v in self.v:
            for h in o.h:
                if h.val < v.lwr:
                    continue
                elif h.val > v.upr:
                    break
                elif h.lwr > v.val or h.upr < v.val:
                    continue
                else:
                    i += [(v.val, h.val)]

        return i

def manhattan(arr):
    return [abs(a[0]) + abs(a[1]) for a in arr]

def solution(data):
    a, b = Segments(data[0]), Segments(data[1])
    d = manhattan(a.find_intersections(b))
    d.sort()
    print(d[0])

with open("in.txt", "r") as f:
    data = f.read()
data = data.strip().split("\n")

solution(data)
