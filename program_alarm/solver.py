#!/usr/bin/env python3

from vm import *

with open("in.txt", "r") as f:
    code = f.read()
code = [int(c) for c in code.strip().split(",")]

vm = VM(code)
for i in range(0, 100):
    for j in range(0, 100):
        vm.reset()
        vm.write(1, i)
        vm.write(2, j)
        vm.run()

        if vm[0] == 19690720:
            print(100 * i + j)
            break
