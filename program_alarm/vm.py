#!/usr/bin/env python3
import copy

class VM:
    def __init__(self, code):
        self.code = copy.deepcopy(code)
        self.memory = copy.deepcopy(code)
        self.pc = 0
        self.instr_map = {1:self.add, 2:self.mult, 99:self.halt}

    def add(self):
        a = self.memory[self.next_code()]
        b = self.memory[self.next_code()]
        self.memory[self.next_code()] = a + b
        return 0

    def mult(self):
        a = self.memory[self.next_code()]
        b = self.memory[self.next_code()]
        self.memory[self.next_code()] = a * b
        return 0

    def halt(self):
        return 1

    def next_code(self):
        ret = self.memory[self.pc]
        self.pc += 1
        return ret

    def write(self, pos, val):
        self.memory[pos] = val

    def __getitem__(self, key):
        return self.memory[key]

    def reset(self):
        self.memory = copy.deepcopy(self.code)
        self.pc = 0

    def run(self):
        while(True):
            try:
                opcode = self.next_code()
                try:
                    if self.instr_map[opcode]():
                        return
                except KeyError:
                    print("Error: unknown opcode " + str(opcode))
                    return -1
            except IndexError:
                print("Error: ran off the edge of memory.")
                return -1
