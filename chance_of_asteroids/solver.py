#!/usr/bin/env python3

from vm import *

with open("in.txt", "r") as f:
    code = f.read()
code = [int(c) for c in code.strip().split(",")]

vm = VM(code)
vm.run()
